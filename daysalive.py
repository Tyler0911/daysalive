
import datetime

def convert_months(months):
    monthvalues = {
    0:31,
    1:28,
    2:31,
    3:30,
    4:31,
    5:30,
    6:31,
    7:31,
    8:30,
    9:31,
    10:30,
    11:31
    } # declare a dictionary to assign months to days
    final_days=0
    for i in range(months):
        final_days+=monthvalues[i] # add the days correlating to months
    return final_days

def convert_time(year=0,months=0,days=0):
    finaldays=convert_months(months) # call the above func
    finaldays+=days # use common sense i'm sure you can figure this out
    year=datetime.datetime.now().year-year # get actual years alive
    finaldays+=year*365.25 # translate years to days
    return finaldays


def main():
    try:
        year = int(input("Year of birth: "))
        month = int(input("Month of birth: "))
        days = int(input("Day of birth: "))
    except TypeError:
        print("Enter a correct value please.")

    print(int(convert_time(year,month,days)))

if __name__ == '__main__':
    main()
